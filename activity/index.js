// console.log('hello world')


let usersArr = [
    'Dwayne Johnson',
    'Steve Austin',
    'Kurt Angle',
    'Dave Bautista'
];

console.log('Original Array:')
console.log(usersArr);

// #3

function addUser(userName){
    usersArr[usersArr.length++] = userName;
}

addUser('John Cena');
console.log(usersArr);

// #4

function findUser(index){
    return usersArr[index];
}

let itemFound = findUser(2);
console.log(itemFound);

// #5

function deleteLastItem(){
    let lastItem = usersArr[usersArr.length - 1];

    usersArr.length--;

    return lastItem;
}

console.log(deleteLastItem());
console.log(usersArr);

// #6

function updateUser(newUserName,index){

    usersArr[index] = newUserName;

}

updateUser('Triple H', 3);

console.log(usersArr);


// #7

function deleteAllUsers(){
    usersArr = [];
}

deleteAllUsers();
console.log(usersArr);


//#8


function checkLength(){

    let result = usersArr.length > 0 ? false : true;
    return result;
}

let isUsersEmpty = checkLength();
console.log(isUsersEmpty);












