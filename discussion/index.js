console.log('hello world');

let studentNumberA = '2020-1923';
let studentNumberB = '2020-1924';
let studentNumberC = '2020-1925';
let studentNumberD = '2020-1926';
let studentNumberE = '2020-1927';


let studentID = ["2020-1923","2020-1924","2020-1925","2020-1926","2020-1927"];

console.log(studentID);

/*  Arrays
    -used to store multiple related values ina single variable.
    -declared using the square brackets [], also known as 'Array literals'

    Syntax:
        let/const arrayName = [elementA, elementB,... elementN]

*/

let grades = [96.8,85.7,93.2,94.6];
console.log(grades);

let computerBrands = ['Acer','Asus', 'Lenovo','Neo','Redfox','Gateway','Toshiba','Fujitsu'];
console.log(computerBrands);

let mixedArr = [12, "Asus", null, undefined, {}]; //not recommended
console.log(mixedArr);

let myTasks = [
    'drink html',
    'eat javascript',
    'inhale css',
    'bake sass'
];
console.log(myTasks);

let city1 = 'Tokyo';
let city2 = 'Manila';
let city3 = 'New York';
let city4 = 'Beijing';

let cities = [city1, city2, city3, city4];
console.log(typeof cities);

let citiesSample = ['Tokyo','Manila','New York','Beijing'];
console.log(typeof citiesSample);

console.log(cities == citiesSample);

/* 
    Array Length
        - to set or get the itms or elements in an array
*/

console.log(myTasks.length); //4

let blankArr = [];
console.log(blankArr);

myTasks.length--

console.log(myTasks.length);
console.log(myTasks);

let theBeatles = ['John','Paul','Ringo','George'];
theBeatles.length++;

theBeatles[4] = 'Yoby';
console.log(theBeatles);

/* 
    Accessing Elements of an Array
    Syntax:
        arrayName[index]
*/

console.log(grades[0]);
console.log(computerBrands[3]);

let lakersLegend = ['Kobe','Shaq','Lebron','Magic','Kareem'];
console.log(lakersLegend[0]);
console.log(lakersLegend[2]);

let currentLaker = lakersLegend[2];
console.log(currentLaker);

console.log('Array before the reassignment');
console.log(lakersLegend);

lakersLegend[2] = "Pau Dasol";

console.log("Array after reassignment");
console.log(lakersLegend);

let bullsLegend = ['Jordan','Pippen','Rodman','Rose','Kukoc'];
let lastElementIndex = bullsLegend.length -1;
console.log(bullsLegend[lastElementIndex])

/* Adding items into an array */
let newArr =[]
console.log(newArr)
console.log(newArr[0])

newArr[0] = "Jenny"
console.log(newArr)
newArr[1] = "Jisoo"
console.log(newArr)

// Mini activity:

/*

Part 1: Adding a value at the end of the array

	-Create a function which is able to receive a single argument and add the input at the end of the superheroes array
	-Invoke and add an argument to be passed to the function
	-Log the superheroes array in the console

	let superHeroes = ['Iron Man', 'Spiderman', 'Captain America']

Part 2: Retrieving an element using a function
	
	-Create a function which is able to receive an index number as a single argument
	-Return the element/item accessed by the index.
	-Create a global variable named heroFound and store/pass the value returned by the function.
	-Log the heroFound variable in the console.

*/

let superHeroes = ['Iron Man', 'Spiderman', 'Captain America'];
console.log(superHeroes);

function addSuperhero(heroName){

    superHeroes[superHeroes.length++] = heroName;

}

addSuperhero('Black Widow');
console.log(superHeroes);

function getHero(index){

    return superHeroes[index];
}

let heroFound = getHero(1);

console.log(heroFound);

for(let index = 0; index < newArr.length; index++){
    console.log(newArr[index]);
}

let numArr = [5,12,26,30,42,50,67,85];

for(let index = 0; index < numArr.length; index++){

    if(numArr[index] % 5 === 0){
        console.log(numArr[index] + ' is divisible by 5');
    }else {
        console.log(numArr[index] + ' is not divisible by 5');
    }
}


/* 
    Multidimensional Arrays
        -arrays within an array

*/

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8'],

];

console.log(chessBoard[1][4]); 
console.log("Pawn moves to: " + chessBoard[7][4]); //e8
















